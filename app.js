const express = require("express")
const app = express()
const bodyParser = require("body-parser")
const jsonParser = bodyParser.json()
require('dotenv').config();

app.get("/", (req,res)=> {
    res.send(`Author : ${process.env.AUTHOR}`)
})


app.post("/add", jsonParser, (req,res)=> {
    console.log(req.body)
    if(req.body) {
        var result = req.body.number_1 + req.body.number_2
        res.send(result.toString())
    } else {
        res.statusMessage("enter valid parameters")
    }
})

app.listen(process.env.PORT, ()=> {
    console.log(`server listening on port ${process.env.PORT}`)
})